"""map.py
Map module for dungeon generation
"""

import random
from const import ROOM, NONE, CORRIDOR

#TODO:
# Finish gen_sane method fix the "not iterable" error
# Suspicions lead me to think that I should rewrite it.

class Map(object):
	count = 0
	rooms = [
	[NONE, NONE, NONE],
	[NONE, NONE, NONE],
	[NONE, NONE, NONE]
	]
	
	def __init__(self):
		""" Generic setup """
		while self.count <= 5:
			self.gen_room()
			self.count += 1
		self.gen_corridors()
		if self.rooms[1][1] != ROOM:
			self.rooms[1][1] = ROOM
					
	def gen_room(self):
		""" Randomly generates a room (1) """
		row = random.randint(0,2)
		index = random.randint(0, 2)
		self.rooms[row][index] = ROOM
	
	def gen_corridors(self):
		""" Generates connecting corridors to isolated rooms (2)"""
		for row in self.rooms:
			#if left and right but no centre rooms
			if (row[0] and row[2]) and (not row[1]):
				row[1] = CORRIDOR
			#if left or right but no centre
			if  row[0] or row[2] and not row[1]:
				row[1] = CORRIDOR
			
	def show_rooms(self):
		return self.rooms

