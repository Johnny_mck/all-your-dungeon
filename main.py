#!/usr/bin/env python

"""main.py
Main python runner script
"""

from map import Map

def main():
	#used for debugging
	map_ = Map()
	return map_.show_rooms()

if __name__ == '__main__':
    i = main()
    print i
